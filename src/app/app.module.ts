import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { RestProvider } from '../providers/rest/rest';

import { HttpClientModule, HttpClient } from '@angular/common/http';

import { HTTP } from '@ionic-native/http';
import { IonicStorageModule } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { FCM } from '@ionic-native/fcm';

import { Camera } from '@ionic-native/camera';

import { FileTransfer } from '@ionic-native/file-transfer';
import { Geolocation } from '@ionic-native/geolocation';
import { HomeworkProvider } from '../providers/homework/homework';
import { CheckNameProvider } from '../providers/check-name/check-name';
import { ProvidersNewsProvider } from '../providers/providers-news/providers-news';
import { ScoreProvider } from '../providers/score/score';
import { SdqProvider } from '../providers/sdq/sdq';
import { TextToSpeech } from '@ionic-native/text-to-speech';


@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    TextToSpeech,
    Geolocation,
    FileTransfer,
    Camera,
    FCM,
    ScreenOrientation,
    InAppBrowser,
    HTTP,
    HttpClient,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    RestProvider,
    HomeworkProvider,
    CheckNameProvider,
    ProvidersNewsProvider,
    ScoreProvider,
    SdqProvider
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class AppModule { }
