import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { FCM } from '@ionic-native/fcm';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = 'LoginPage';
  pages: Array<{ title: string, component: any }>;


  constructor(public platform: Platform, public statusBar: StatusBar,
    private storage: Storage, private splashScreen: SplashScreen,
    private fcm: FCM) {

    this.initializeApp();

    platform.ready().then(() => {

          // let status bar overlay webview
    this.statusBar.overlaysWebView(true);
    // set status bar to white
    this.statusBar.backgroundColorByHexString('#FFCA18');

      // statusBar.styleDefault();
      splashScreen.hide();
    });

    window.addEventListener("keyboardDidShow", () => {
      document.activeElement.scrollIntoView(false);
      const elem: HTMLCollectionOf<Element> = document.getElementsByClassName("scroll-content");
      if (elem !== undefined && elem.length > 0) {
        elem[elem.length - 1].scrollTop += 40;
      }
    });

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.splashScreen.hide();

      this.fcm.getToken().then(token => {
        console.log("qqqqqqqstoken : " + token);
        this.storage.set('token_st', token);
      });

      this.nav.setRoot("LoginPage");
    });
  }

  public selectmode(mode: boolean) {
    if (mode) {
      this.pages = [
        { title: 'หน้าหลัก', component: 'HomePage' },
        { title: 'วิชาทั้งหมด', component: 'HomeworkPage' },
        { title: 'ข่าวสารทั้งหมด', component: 'NewsPage' },
        { title: 'รายการการบ้านทั้งหมด', component: 'AllHomeworkPage' },
        { title: 'สถานะการเข้าเรียน', component: 'StatusPage' },
        { title: 'ตารางเรียน/สอบ', component: 'SchedulePage' },
        { title: 'แบบประเมิน SDQ', component: 'SdqPage' },
        { title: 'เปลี่ยนรหัสผ่าน', component: 'ChangepassMenuPage' },
        { title: 'ออกจากระบบ', component: 'LoginPage' }
      ];
    } else {
      this.pages = [
        { title: 'หน้าหลัก', component: 'ParentHomePage' },
        { title: 'วิชาทั้งหมด', component: 'ParentHomeworkPage' },
        { title: 'ข่าวสารทั้งหมด', component: 'ParentNewsPage' },
        { title: 'รายการการบ้านทั้งหมด', component: 'ParentAllHomeworkPage' },
        { title: 'สถานะการเข้าเรียน', component: 'ParentStatusPage' },
        { title: 'ตารางเรียน/สอบ', component: 'ParentSchedulePage' },
        { title: 'แบบประเมิน SDQ', component: 'ParentSdqPage' },
        { title: 'เปลี่ยนรหัสผ่าน', component: 'ParentChangepassMenuPage' },
        { title: 'ออกจากระบบ', component: 'LoginPage' }
      ];
    }
  }

  openPage(page: any): void {
    if (page.title == 'ออกจากระบบ') {
      this.storage.remove('userid')
    }
    this.nav.setRoot(page.component);
  }
}

