import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the ListVerbPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-verb',
  templateUrl: 'list-verb.html',
})
export class ListVerbPage {

  courseid: any;
  course_name: any;
  roomid: any
  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;
  IsEng: any;
  v3List: any;
  verblist: any
  noVerblist: boolean = false

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage,
    public restProvider: RestProvider) {

    this.courseid = this.navParams.get('courseid');

  }


  ionViewDidLoad() {

    this.storage.get('student_img').then((val) => {
      this.student_img = val;
      this.storage.get('student_level').then((val) => {
        this.student_level = val;
        this.storage.get('userid').then((val) => {
          this.userid = val;
          this.storage.get('schoolid').then((val) => {
            this.schoolid = val;
            this.storage.get('roomid').then((val) => {
              this.roomid = val
              this.restProvider.getV3List(this.courseid, this.roomid).subscribe(result => {
                console.log('list สาระสำคัญอื่นๆ', result);
                if (result[0].id == '') {
                  this.noVerblist = true
                }
                this.verblist = result

              })
            })

          })
        })
      })
    })

  }

  clickList(id) {
    this.navCtrl.push("VerbPage", { 'id': id });
  }

  VerbPage(verbId) {
    this.navCtrl.push("VerbPage", { 'verbId': verbId });
  }

  back() {
    this.navCtrl.push("EnglishPage");
  }

}
