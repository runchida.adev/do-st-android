import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { HTTP } from '../../../node_modules/@ionic-native/http';

/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  username: string;
  tel: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public restProvider: RestProvider,
    public https: HTTP,
    private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }

  resetpassword() {

    // console.log(this.user);
    // console.log(this.tel);

    this.restProvider.resetpassword(this.username, this.tel).subscribe((result) => {
      let alert = this.alertCtrl.create({
        message: 'บันทึกเรียบร้อย'
      })
      alert.present()
      setTimeout(() => {
        alert.dismiss()
        this.navCtrl.setRoot('LoginPage')
      }, 1000);

      console.log(result);

    });


  }

}
