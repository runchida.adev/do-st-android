import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the ParentListVocabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parent-list-vocab',
  templateUrl: 'parent-list-vocab.html',
})
export class ParentListVocabPage {

  courseid: any;
  course_name: any;

  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;
  IsEng: any;
  roomid: any;
  v3List: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage,
    public restProvider: RestProvider) {

    this.courseid = this.navParams.get('courseid');

  }


  ionViewDidLoad() {
    this.storage.get('student_img').then((val) => {
      this.student_img = val;
      this.storage.get('student_level').then((val) => {
        this.student_level = val;
        this.storage.get('userid').then((val) => {
          this.userid = val;
          this.storage.get('schoolid').then((val) => {
            this.schoolid = val;
            this.storage.get('roomid').then((val) => {
              this.roomid = val;

              // console.log('roomid', this.roomid)
              // console.log('courseid', this.courseid)

              this.restProvider.getVocabList(this.courseid, this.roomid).subscribe(result => {
                // console.log('list ใบคำศัพท์: ', result)
                this.v3List = result;
              });
            })
          })
        })
      })
    })


  }

  clickList(id) {
    console.log(id)
    this.navCtrl.push("ParentVocabularyPage", { 'id': id });
  }

  back() {
    this.navCtrl.push("ParentEnglishPage");
  }




}
