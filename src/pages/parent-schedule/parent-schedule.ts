import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { RestProvider } from '../../providers/rest/rest';
import { HTTP } from '../../../node_modules/@ionic-native/http';
import { InAppBrowser } from '../../../node_modules/@ionic-native/in-app-browser';

/**
 * Generated class for the ParentSchedulePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parent-schedule',
  templateUrl: 'parent-schedule.html',
})
export class ParentSchedulePage {


  height: any;
  width: any;
  image = ['schedule.jpg'];
  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider: RestProvider,
    public http: HTTP, private storage: Storage,
    public platform: Platform, private iab: InAppBrowser,
    public modalCtrl: ModalController) {

    this.width = '99%';

    platform.ready().then((readySource) => {
      this.height = (platform.height() * 70) / 100;
      this.height = platform.height() - this.height;
      this.height = this.height + 'px';
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentSchedulePage');
  }


  save(urlImg) {
    let browser = this.iab.create(urlImg, '_blank', {
      closebuttoncaption: "Exit",
    });
  }

  ln: string;
  ex: string;
  ionViewDidEnter() {
    this.storage.get('userid').then((val) => {
      this.userid = val;
      this.storage.get('schoolid').then((val) => {
        this.schoolid = val;
        this.storage.get('student_img').then((val) => {
          this.student_img = val;
          this.storage.get('student_level').then((val) => {
            this.student_level = val;
            this.restProvider.getSchedule(this.userid).subscribe(result => {
              this.ln = result.schedule_img;
              this.ex = result.exam_img;
            });
          })
        })
      })
    })
  }




}
