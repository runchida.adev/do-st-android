import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentSchedulePage } from './parent-schedule';

@NgModule({
  declarations: [
    ParentSchedulePage,
  ],
  imports: [
    IonicPageModule.forChild(ParentSchedulePage),
  ],
})
export class ParentSchedulePageModule {}
