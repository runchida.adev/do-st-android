import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { new_list } from '../../models/news_list';
import { Storage } from '@ionic/storage';
import { InAppBrowser } from '../../../node_modules/@ionic-native/in-app-browser';
import { HTTP } from '../../../node_modules/@ionic-native/http';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the ParentNewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parent-news',
  templateUrl: 'parent-news.html',
})
export class ParentNewsPage {

  height: any;
  width: any;
  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;
  list_news: new_list;
  grade: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public platform: Platform,
    public restProvider: RestProvider, public http: HTTP,
    private storage: Storage, private iab: InAppBrowser) {

    this.width = '100%';
    platform.ready().then((readySource) => {
      this.height = (platform.height() * 70) / 100;
      this.height = platform.height() - this.height;
      this.height = this.height + 'px';
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentNewsPage');
  }


  ionViewDidEnter() {

    this.storage.get('userid').then((val) => {
      this.userid = val;
      this.storage.get('schoolid').then((val) => {
        this.schoolid = val;
        this.storage.get('student_img').then((val) => {
          this.student_img = val;
          this.storage.get('student_level').then((val) => {
            this.student_level = val;
            this.storage.get('grade').then((val) => {
              this.grade = val;
              this.restProvider.getNews(this.schoolid, "pa", this.grade).subscribe(result => {
                this.list_news = result;
                console.log(result);
              });
            })
          })
        })
      })
    })
  }

  detailnews(newsid, news_type, news_detail) {

    if (news_type == "survey") {
      let browser = this.iab.create('http://13.250.56.205/doedu/survey.aspx?schoolid=' + this.schoolid +
        '&userid=' + this.userid + '&surveyid=' + news_detail + '&who=pa', '_blank', {
          closebuttoncaption: "Exit",
        });
    }

    else {
      this.navCtrl.push("ParentDetailNewsPage", {
        'schoolid': this.schoolid,
        'newsid': newsid
      });
    }

  }



}
