import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SdqProvider } from '../../providers/sdq/sdq';
import { HTTP } from '../../../node_modules/@ionic-native/http';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ParentSdqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parent-sdq',
  templateUrl: 'parent-sdq.html',
})
export class ParentSdqPage {

  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;
  courseList: any;
  qusindex: number = 0;
  qusnow: any;

  question_number: any;
  flagOpen: any;

  hide: any = false;
  stdList: Array<any> = [];
  renderList: Array<any> = [];

  clickAns: any = "null";
  qus: string[] = [
    'ห่วงใยความรู้สึกคนอื่น',
    'ไม่อยู่นิ่ง นั่งนิ่งๆไม่ได้',
    'มักจะบ่นว่าปวดศีรษะ ปวดท้อง หรือไม่สบาย',
    'เต็มใจแบ่งปันสิ่งของให้เพื่อน (ขนม ของเล่น ดินสอ เป็นต้น)',
    'มักจะอาละวาด หรือโมโหร้าย',
    'ค่อนข้างแยกตัว ชอบเล่นคนเดียว',
    'เชื่อฟัง มักจะทำตามที่ผู้ใหญ่ต้องการ',
    'กังวลใจหลายเรื่อง ดูวิตกกังวลเสมอ',
    'เป็นที่พึ่งได้เวลาที่คนอื่นเสียใจ อารมณ์ไม่ดี หรือไม่สบายใจ',
    'อยู่ไม่สุข วุ่นวายอย่างมาก',
    'มีเพื่อนสนิท',
    'มักมีเรื่องทะเลาวิวาทกับเด็กอื่น หรือรังแกเด็กอื่น',
    'ดูไม่มีความสุข ท้อแท้ ร้องไห้บ่อย',
    'เป็นที่ชื่นชอบของเพื่อน',
    'วอกแวกง่าย สมาธิสั้น',
    'เครียด ไม่ยอมห่างเวลาอยู่ในสถานการณ์ที่ไม่คุ้น และขาดความเชื่อมั่นในตนเอง',
    'ใจดีกับเด็กที่เล็กกว่า',
    'ชอบโกหก หรือขี้โกง',
    'ถูกเด็กคนอื่นล้อเลียนหรือรังแก',
    'ชอบอาสาช่วยเหลือคนอื่น (พ่อ แม่ ครู และเด็กคนอื่น)',
    'คิดก่อนทำ',
    'ขโมยของที่บ้าน ที่โรงเรียน หรือที่อื่น',
    'เข้ากับผู้ใหญ่ได้ดีกว่าเด็กวัยเดียวกัน',
    'ขี้กลัว รู้สึกหวาดกลัวได้ง่าย',
    'ทำงานได้จนเสร็จ มีความตั้งใจในการทำงาน',
    'โดยรวมคุณคิดว่าเด็กมีปัญหาในด้านใดด้านหนึ่งต่อไปนี้หรือไม่ ด้านอารมณ์ ด้านสมาธิ ด้านพฤติกรรม หรือความสามารถเข้ากับผู้อื่น',
    'ปัญหานี้เกิดขึ้นมานานเท่าไหร่แล้ว',
    'ปัญหานี้รบกวนชีวิตประจำวันของเธอในด้านต่างๆ ต่อไปนี้หรือไม่(การคบเพื่อน)',
    'ปัญหานี้รบกวนชีวิตประจำวันของเธอในด้านต่างๆ ต่อไปนี้หรือไม่(การเรียนในห้องเรียน)',
    'ปัญหาของเด็กทำให้คุณหรือชั้นเรียนเกิดความยุ่งยากหรือไม่'
  ];

  ans: string[] = [
    'ไม่จริง',
    'อาจจะจริง',
    'จริง',
    'null'
  ];


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public http: HTTP, private storage: Storage,
    public SdqProvider: SdqProvider) {
  }

  ionViewDidLoad() {
    this.storage.get('userid').then((val) => {
      this.userid = val;
      this.storage.get('schoolid').then((val) => {
        this.schoolid = val;
        this.storage.get('student_img').then((val) => {
          this.student_img = val;
          this.storage.get('student_level').then((val) => {
            this.student_level = val;
            this.SdqProvider.getQus_pa(this.userid).subscribe((result) => {
              console.log('sdq', result);
              console.log('sdq no. : ', result[0].question_number);

              this.question_number = result[0].question_number;
              this.flagOpen = result[0].flagOpen;
              console.log('Flag ', this.flagOpen);
              this.qusindex = (result[0].question_number - 1);
              this.qusnow = this.qus[this.qusindex];

              console.log("qus", this.qusindex + " " + this.qusnow);

            })
          })
        })
      })
    })
  }
  next(newindex: number) {
    if (this.clickAns != "null") {

      this.qusindex = newindex;
      this.qusnow = this.qus[this.qusindex];

      if (newindex < 25) {
        this.hide = false;
      } else {
        this.hide = true;
        if (newindex == 25) {
          this.popArray();
          this.ans.push("ไม่");
          this.ans.push("ใช่ มีปัญหาเล็กน้อย");
          this.ans.push("ใช่ มีปัญหาชัดเจน");
          this.ans.push("ใช่ มีปัญหาอย่าง");
        }
        if (newindex == 26) {
          this.popArray();
          this.ans.push("น้อยกว่าหนึ่งเดือน");
          this.ans.push("หนึ่งถึงห้าเดือน");
          this.ans.push("หกถึงสิบสองเดือน");
          this.ans.push("มากกว่าหนึ่งปี");
        }
        if (newindex == 27) {
          this.popArray();
          this.ans.push("ไม่");
          this.ans.push("เล็กน้อย");
          this.ans.push("ค่อนข้างมาก");
          this.ans.push("มาก");
        }

        if (newindex == 30) {
          this.navCtrl.setRoot('ParentHomePage');
        }
      }

      this.storage.get('userid').then((val) => {
        this.userid = val;
        this.storage.get('schoolid').then((val) => {
          this.schoolid = val;

          this.SdqProvider.clickAns_pa(this.userid, this.userid, this.schoolid, newindex.toString(), this.clickAns).subscribe((result) => {
            console.log('sdq Ans', result);
          })

          console.log("", newindex.toString());
        })
      })

    }
  }

  popArray() {
    this.ans.pop();
    this.ans.pop();
    this.ans.pop();
    this.ans.pop();
  }

  clickC(ans) {
    this.clickAns = ans;
    console.log("ans", ans);

  }




}
