import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TopicScoreAllPage } from './topic-score-all';

@NgModule({
  declarations: [
    TopicScoreAllPage,
  ],
  imports: [
    IonicPageModule.forChild(TopicScoreAllPage),
  ],
})
export class TopicScoreAllPageModule {}
