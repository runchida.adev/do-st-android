import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ScoreProvider } from '../../providers/score/score';
import { HTTP } from '../../../node_modules/@ionic-native/http';

/**
 * Generated class for the TopicScoreAllPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-topic-score-all',
  templateUrl: 'topic-score-all.html',
})
export class TopicScoreAllPage {

  id: any;
  subject: any;
  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;
  roomid: string
  courseid: any;
  course_name: any;
  allScore: any
  totalScore: any;
  score: any;
  normalScore: Array<any> = []
  midtermScore: any
  finalScore: any
  IsSeeAll: any
  teacherid: any;
  sub_score_id: any;
  renderList: Array<any> = [];
  scoreList: Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage, public scoreProvider: ScoreProvider, public http: HTTP) {

    this.sub_score_id = this.navParams.get('sub_score_id');

  }


  ionViewDidLoad() {

    this.storage.get('userid').then((val) => {
      this.userid = val;
      this.storage.get('schoolid').then((val) => {
        this.schoolid = val;
        this.storage.get('student_img').then((val) => {
          this.student_img = val;
          this.storage.get('student_level').then((val) => {
            this.student_level = val;
            this.storage.get('roomid').then((val) => {
              this.roomid = val

              this.scoreProvider.getScoreClassAll(this.sub_score_id, this.schoolid).subscribe((result) => {
                console.log('score list', result);
                this.scoreList = result
                this.scoreList.forEach(element => {
                  if (element.isSent == 'n') {
                    this.renderList.push(element)
                  }
                });
              })


            })
          })
        })
      })
    })

  }

  back() {
    this.navCtrl.setRoot('HomePage');
  }






}
