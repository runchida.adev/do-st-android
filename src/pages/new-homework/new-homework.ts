import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FileTransfer, FileTransferObject } from '../../../node_modules/@ionic-native/file-transfer';
import { Camera, CameraOptions } from '../../../node_modules/@ionic-native/camera';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the NewHomeworkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-homework',
  templateUrl: 'new-homework.html',
})
export class NewHomeworkPage {


  userid: any;
  student_level: string;
  student_img: string;
  schoolid: any;
  roomid: string;
  homeworkName: string;
  detail: string;
  dateOfEvent: any;
  courseid: any;
  alert_before: any;
  doc: any;
  createid: any;
  updateid: any;
  isactive = 'y';
  url_img: any;
  homework_img: string;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage
    , private camera: Camera, public restProvider: RestProvider
    , private transfer: FileTransfer, public alertCtrl: AlertController) {

    this.courseid = this.navParams.get("courseid");
    this.schoolid = this.navParams.get("schoolid");
    this.createid = this.navParams.get("userid");
    this.updateid = this.navParams.get("userid");

  }

  ionViewDidEnter() {
    this.storage.get('student_img').then((val) => {
      this.student_img = val;
      this.storage.get('student_level').then((val) => {
        this.student_level = val;
        this.storage.get('userid').then((val) => {
          this.userid = val;
          this.storage.get('schoolid').then((val) => {
            this.schoolid = val;
          })
        })
      })
    })

  }

  ionViewDidLoad() {

    this.storage.get('roomid').then((val) => { this.roomid = val })

  }

  edithomework() {
    this.navCtrl.push("EditHomeworkPage");
  }

  back() {
    this.navCtrl.push("EnglishPage");
  }


  test() {

    const options: CameraOptions = {
      quality: 50,

      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;

      const fileTransfer: FileTransferObject = this.transfer.create();
      fileTransfer.upload(base64Image, 'http://13.250.56.205/du_service/api/uplodeImgHomework').then((data) => {

        this.doc = data.response
        this.doc = this.doc.substr(1).slice(0, -1);
        console.log('data', JSON.stringify(data));
        console.log('imageUrl', this.doc);
        this.restProvider.tmppictureHomework(this.doc, this.userid).subscribe((result) => {
          console.log('result', JSON.stringify(result));
          this.homework_img = result
        })
      })
    }, (err) => {
      console.log(err);
    });
  }

  saveHomework() {

    if (this.homework_img == null) {
      this.homework_img = "";
      // this.presentAlert("กรุณาเลือกรูปภาพก่อน");
    }

    this.restProvider.saveHomework(this.courseid, this.schoolid, this.createid, this.updateid,
      this.homeworkName, this.dateOfEvent, this.detail, this.homework_img, this.alert_before,
      this.roomid, this.isactive).subscribe((result) => {
        // this.storage.set('userid', result.userid);

        console.log(JSON.stringify(result));
        console.log(result.return_desc);
        if (result.return_desc) {
          this.presentAlert("สร้างการบ้านเสร็จสิ้น");
          this.navCtrl.setRoot("HomePage");
        }
      });

  }

  async presentAlert(message) {
    const alert = await this.alertCtrl.create({
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }


}
