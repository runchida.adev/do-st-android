import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewHomeworkPage } from './new-homework';

@NgModule({
  declarations: [
    NewHomeworkPage,
  ],
  imports: [
    IonicPageModule.forChild(NewHomeworkPage),
  ],
})
export class NewHomeworkPageModule {}
