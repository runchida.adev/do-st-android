import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentVocabularyPage } from './parent-vocabulary';

@NgModule({
  declarations: [
    ParentVocabularyPage,
  ],
  imports: [
    IonicPageModule.forChild(ParentVocabularyPage),
  ],
})
export class ParentVocabularyPageModule {}
