import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TextToSpeech } from '../../../node_modules/@ionic-native/text-to-speech';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the ParentVocabularyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parent-vocabulary',
  templateUrl: 'parent-vocabulary.html',
})
export class ParentVocabularyPage {

  userid: any
  courseId: any
  student_level: string;
  student_img: string;
  id: any;
  title: any
  verb_img: any
  vocabList: any;
  schoolid: any;
  roomid: any;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage, public restProvider: RestProvider,
    private tts: TextToSpeech) {

    this.id = navParams.get('id')

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentVocabularyPage');
  }

  ionViewDidEnter() {

    this.storage.get('student_img').then((val) => {
      this.student_img = val;
      this.storage.get('student_level').then((val) => {
        this.student_level = val;
        this.storage.get('userid').then((val) => {
          this.userid = val;
          this.storage.get('schoolid').then((val) => {
            this.schoolid = val;
            this.storage.get('roomid').then((val) => {
              this.roomid = val
              this.restProvider.getVocabListDetail(this.id).subscribe(result => {
                this.vocabList = result;
              })
            })
          })
        })
      })
    })
  }

  speaText(texToSpeek) {
    console.log('speak text', texToSpeek);

    this.tts.speak({
      text: texToSpeek,
      rate: 1
    })
  }

  back() {
    this.navCtrl.setRoot('ParentEnglishPage');
  }




}
