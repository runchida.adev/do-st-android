import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, MenuController, AlertController } from 'ionic-angular';
import { MyApp } from '../../app/app.component';
import { Storage } from '@ionic/storage';
import { RestProvider } from '../../providers/rest/rest';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  mode: boolean;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public platform: Platform, private menu: MenuController, 
    private storage: Storage , public myapp : MyApp, public restProvider: RestProvider,
    public alertCtrl: AlertController,private screenOrientation: ScreenOrientation) {

    this.mode;

    platform.registerBackButtonAction(() => {
      let view = this.navCtrl.getActive();
      if (view.component.name == "HomePage") {
      } else if (view.component.name == "LoginPage") {
        this.platform.exitApp();
      } else {
        this.navCtrl.pop({});
      }
    });
  }

  @ViewChild("username") mUsername;
  @ViewChild("password") mPassword;

  ionViewDidEnter() {

    this.menu.swipeEnable(false);
    this.storage.get('userid').then((val) => {
      console.log('login page user id ', val);
      if (val != null && val != "Username หรือ Password ไม่ถูกต้อง") {

        this.storage.get('mode').then((mode) => {
          if(mode == "st"){
            this.myapp.selectmode(true);
            this.navCtrl.setRoot("HomePage", {userId: val});
          }else if(mode == "pa"){
            this.myapp.selectmode(false);
            this.navCtrl.setRoot("ParentHomePage", {userId: val});
          }
        });

      }
    })
    console.log("come");
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  checkLogin() {

    let username = this.mUsername.value;
    let password = this.mPassword.value;

    this.menu.swipeEnable(true);
    this.restProvider.login(username, password ).subscribe(result => {
      console.log(result);

      this.storage.set('userid', result.userid);

      if (result.returncode == "1000") {

        if(result.role == "st"){
          this.mode = true;
          this.myapp.selectmode(this.mode);
          this.storage.set('mode', "st");
        }else if(result.role == "pa"){
          this.mode = false;
          this.myapp.selectmode(this.mode);
          this.storage.set('mode', "pa");
        }

        if(this.mode){
          this.navCtrl.setRoot("HomePage", {
            userId: result.userid
          });
        }else{
          this.navCtrl.setRoot("ParentHomePage", {
            userId: result.userid
          });
        }
      } else {
        let alert = this.alertCtrl.create({
          message: result.userid,
          buttons: ['ตกลง']
        })
        alert.present()
      }
    });
  }

  getCurrentScreenOreintation() {
    console.log(this.screenOrientation.type);
  }

  portrian() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  change_password() {
    this.navCtrl.push("ChangePasswordPage");
  }



}
