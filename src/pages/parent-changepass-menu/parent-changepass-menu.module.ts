import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentChangepassMenuPage } from './parent-changepass-menu';

@NgModule({
  declarations: [
    ParentChangepassMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(ParentChangepassMenuPage),
  ],
})
export class ParentChangepassMenuPageModule {}
