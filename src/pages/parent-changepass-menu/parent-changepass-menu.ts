import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the ParentChangepassMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parent-changepass-menu',
  templateUrl: 'parent-changepass-menu.html',
})
export class ParentChangepassMenuPage {

  [x: string]: any;
  username: string;
  tel: string;
  newpassword: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider: RestProvider, public AlertController: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentChangepassMenuPage');
  }

  change_password() {

    this.restProvider.change_password_pa(this.username, this.tel, this.newpassword).subscribe(async (result) => {
      console.log(result);
      console.log("retuencode : " + result.returncode + "\n" + "desc : " + result.desc);
      if (result.returncode == "3810") {
        let alert = this.AlertController.create({
          message: result.desc,
          buttons: ['รับทราบ']
        })
        alert.present()
      } else if (result.returncode == "3800") {
        let alert = this.AlertController.create({
          message: result.desc,
          buttons: ['รับทราบ']
        })
        alert.present()
      } else if (result.returncode == "1000") {
        let alert = this.AlertController.create({
          message: "เปลี่ยนรหัสผ่านใหม่สำเร็จ",
          buttons: ['รับทราบ']
        })
        alert.present()
        this.navCtrl.setRoot("ParentHomePage");
      }

    });
  }

}
