import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TopicScorePage } from './topic-score';

@NgModule({
  declarations: [
    TopicScorePage,
  ],
  imports: [
    IonicPageModule.forChild(TopicScorePage),
  ],
})
export class TopicScorePageModule {}
