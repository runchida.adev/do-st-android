import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { RestProvider } from '../../providers/rest/rest';
import { ScoreProvider } from '../../providers/score/score';
import { HTTP } from '../../../node_modules/@ionic-native/http';

/**
 * Generated class for the TopicScorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-topic-score',
  templateUrl: 'topic-score.html',
})
export class TopicScorePage {

  course_name: any;
  userid: any;
  student_level: string;
  student_img: string;
  courseid: any;
  schoolid: any;
  roomid: any;
  teacherid: any;
  sub_score_id: any;
  stdList: Array<any> = [];
  renderList: Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage,
    public restProvider: RestProvider, public scoreProvider: ScoreProvider,
    public http: HTTP) {

    this.courseid = this.navParams.get('courseid');
    this.teacherid = navParams.get('teacherid');

  }


  ionViewDidLoad() {

    this.storage.get('student_img').then((val) => {
      this.student_img = val;
      this.storage.get('student_level').then((val) => {
        this.student_level = val;
        this.storage.get('userid').then((val) => {
          this.userid = val;
          this.storage.get('schoolid').then((val) => {
            this.schoolid = val;
            this.storage.get('roomid').then((val) => {
              this.roomid = val;
              console.log(this.courseid + " " + this.schoolid + " " + this.roomid + " " + this.teacherid);

              this.scoreProvider.getStd(this.courseid, this.schoolid, this.roomid, this.teacherid).subscribe((result) => {
                console.log('topic list', result);

                this.stdList = result
                this.stdList.forEach(element => {
                  if (element.isSent == 'n') {
                    this.renderList.push(element)
                  }
                });
              })

            })
          })
        })
      })
    })

  }

  back() {
    this.navCtrl.setRoot('HomePage');
  }

  allScore(sub_score_id) {
    this.navCtrl.setRoot("TopicScoreAllPage", { "sub_score_id": sub_score_id });
  }





}
