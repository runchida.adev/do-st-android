import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TotalScorePage } from './total-score';

@NgModule({
  declarations: [
    TotalScorePage,
  ],
  imports: [
    IonicPageModule.forChild(TotalScorePage),
  ],
})
export class TotalScorePageModule {}
