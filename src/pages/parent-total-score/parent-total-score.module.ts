import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentTotalScorePage } from './parent-total-score';

@NgModule({
  declarations: [
    ParentTotalScorePage,
  ],
  imports: [
    IonicPageModule.forChild(ParentTotalScorePage),
  ],
})
export class ParentTotalScorePageModule {}
