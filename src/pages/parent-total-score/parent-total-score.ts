import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomeworkProvider } from '../../providers/homework/homework';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ParentTotalScorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parent-total-score',
  templateUrl: 'parent-total-score.html',
})
export class ParentTotalScorePage {

  id: any;
  subject: any;

  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;
  roomid: string
  courseid: any;
  course_name: any;

  allScore: any

  totalScore: any;
  score: any;

  normalScore: Array<any> = []
  midtermScore: any
  finalScore: any
  IsSeeAll: any
  teacherid: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage,
    private _service: HomeworkProvider) {

    this.courseid = navParams.get('courseid');
    this.course_name = navParams.get('course_name');
    this.teacherid = navParams.get('teacherid');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentTotalScorePage');
  }


  ionViewDidEnter() {
    this.storage.get('userid').then((val) => {
      this.userid = val;
      this.storage.get('schoolid').then((val) => {
        this.schoolid = val;
        this.storage.get('student_img').then((val) => {
          this.student_img = val;
          this.storage.get('student_level').then((val) => {
            this.student_level = val;
            this.storage.get('roomid').then((val) => {
              this.roomid = val

              this._service.getAllScore(this.userid, this.schoolid, this.roomid, this.courseid).subscribe(result => {
                console.log('get all score', result);
                this.allScore = result
                this.allScore.forEach(element => {
                  if (element.scorename == 'Midterm') {
                    this.midtermScore = element.score
                  }
                  if (element.scorename == 'Final') {
                    this.finalScore = element.score
                  }
                  if (element.scorename != 'Midterm' && element.scorename != 'Final') {
                    this.normalScore.push(element)
                  }

                  this.IsSeeAll = element.IsSeeAll;

                });
              })

            })
          })
        })
      })
    })

  }

  back() {
    this.navCtrl.setRoot('HomePage');
  }

  topic_score() {
    this.navCtrl.setRoot("TopicScorePage", { 'courseid': this.courseid, 'teacherid': this.teacherid });
  }




}
