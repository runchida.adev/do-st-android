import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomeworkProvider } from '../../providers/homework/homework';

/**
 * Generated class for the DetailEnglishPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-english',
  templateUrl: 'detail-english.html',
})
export class DetailEnglishPage {


  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;

  courseid: any;
  course_name: any;

  homeworkList: any;
  renderList: Array<any> = []

  noHomework: boolean = false


  constructor(public navCtrl: NavController,
    public navParams: NavParams, private storage: Storage,
    private _service: HomeworkProvider,
    public loadingController: LoadingController) {

    this.courseid = navParams.get('courseid');
    this.course_name = navParams.get('course_name');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailEnglishPage');
  }

  ionViewDidEnter() {
    this.renderList = []
    this.storage.get('student_img').then((val) => {
      this.student_img = val;
      this.storage.get('student_level').then((val) => {
        this.student_level = val;
        this.storage.get('userid').then((val) => {
          this.userid = val;
          this.storage.get('schoolid').then((val) => {
            this.schoolid = val;

            this._service.getHomework(this.courseid, this.userid, this.schoolid).subscribe(result => {
              this.homeworkList = result;
              // console.log(result[0].id);
              if (result[0].id == '') {
                this.noHomework = true
              }
              console.log('homework list', this.homeworkList);
              this.homeworkList.forEach(element => {
                if (element.isSent == 'n') {
                  this.renderList.push(element)
                }
              });
            });
          })
        })
      })
    })
  }

  detailHomework(homeworkId) {
    console.log('prepare go to detail homework id', homeworkId);
    this.navCtrl.push("DetailHomeworkPage", { 'homeworkid': homeworkId, 'courseid': this.courseid })

  }

  back() {
    this.navCtrl.setRoot("EnglishPage");
  }


}
