import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailEnglishPage } from './detail-english';

@NgModule({
  declarations: [
    DetailEnglishPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailEnglishPage),
  ],
})
export class DetailEnglishPageModule {}
