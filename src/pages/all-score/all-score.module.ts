import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllScorePage } from './all-score';

@NgModule({
  declarations: [
    AllScorePage,
  ],
  imports: [
    IonicPageModule.forChild(AllScorePage),
  ],
})
export class AllScorePageModule {}
