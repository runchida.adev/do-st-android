import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdjmapPage } from './adjmap';


@NgModule({
  declarations: [
    AdjmapPage,
  ],
  imports: [
    IonicPageModule.forChild(AdjmapPage),
  ],
})
export class AdjmapPageModule { }
