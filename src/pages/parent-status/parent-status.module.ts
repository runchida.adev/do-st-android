import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentStatusPage } from './parent-status';

@NgModule({
  declarations: [
    ParentStatusPage,
  ],
  imports: [
    IonicPageModule.forChild(ParentStatusPage),
  ],
})
export class ParentStatusPageModule {}
