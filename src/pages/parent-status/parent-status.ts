import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { HTTP } from '../../../node_modules/@ionic-native/http';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ParentStatusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parent-status',
  templateUrl: 'parent-status.html',
})
export class ParentStatusPage {

  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider: RestProvider,
    public http: HTTP, private storage: Storage) {
  }

  cList: any;

  ionViewDidLoad() {


  }

  ionViewDidEnter() {

    // let Sid = this.navParams.get("schoolid");
    this.storage.get('userid').then((val) => {
      this.userid = val;
      this.storage.get('schoolid').then((val) => {
        this.schoolid = val;
        this.storage.get('student_img').then((val) => {
          this.student_img = val;
          this.storage.get('student_level').then((val) => {
            this.student_level = val;

            this.restProvider.getStatus(this.userid, this.schoolid).subscribe(result => {
              // console.log(JSON.stringify(result));
              this.cList = result;

              console.log(this.cList);
            });

          })
        })
      })
    })
  }




}
