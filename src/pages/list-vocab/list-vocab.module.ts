import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListVocabPage } from './list-vocab';

@NgModule({
  declarations: [
    ListVocabPage,
  ],
  imports: [
    IonicPageModule.forChild(ListVocabPage),
  ],
})
export class ListVocabPageModule {}
