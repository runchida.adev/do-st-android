import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IdTeacherPage } from './id-teacher';

@NgModule({
  declarations: [
    IdTeacherPage,
  ],
  imports: [
    IonicPageModule.forChild(IdTeacherPage),
  ],
})
export class IdTeacherPageModule {}
