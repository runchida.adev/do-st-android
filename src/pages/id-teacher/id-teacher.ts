import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CheckNameProvider } from '../../providers/check-name/check-name';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the IdTeacherPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-id-teacher',
  templateUrl: 'id-teacher.html',
})
export class IdTeacherPage {

  userid: any;
  student_level: string;
  student_img: string;

  schoolid: any;
  courseid: any;
  roomid: any;
  password: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public _serv: CheckNameProvider,
    private storage: Storage) {


    this.courseid = this.navParams.get('courseid');
    console.log(this.courseid)

  }

  ionViewDidLoad() {
    this.storage.get('student_img').then((val) => { this.student_img = val })
    this.storage.get('schoolid').then((val) => { this.schoolid = val })
    this.storage.get('roomid').then((val) => { this.roomid = val })
  }

  ionViewDidEnter() {
    this.storage.get('student_img').then((val) => {
      this.student_img = val;
      this.storage.get('student_level').then((val) => {
        this.student_level = val;
        this.storage.get('userid').then((val) => {
          this.userid = val;
          this.storage.get('schoolid').then((val) => {
            this.schoolid = val;
          })
        })
      })
    })
  }

  back() {
    this.navCtrl.push("EnglishPage");
  }

  setPassword(pass) {
    this.password = pass;
  }

  ChecknamePage() {

    this._serv.getInfo(this.schoolid, this.courseid, this.roomid, this.password).subscribe(result => {
      console.log(result[0]["desc"]);

      if (result[0]["desc"] == "success") {
        this.navCtrl.push("ChecknamePage", { 'courseid': this.courseid, 'teacherid': result[0]["teacherid"] });
      }
      else {
        alert("ไม่สามารถเข้าเช็คชื่อได้");
        this.navCtrl.setRoot("HomeworkPage");
      }


    })
  }


}
