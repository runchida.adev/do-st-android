import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { HTTP } from '../../../node_modules/@ionic-native/http';
import { Camera, CameraOptions } from '../../../node_modules/@ionic-native/camera';
import { FileTransfer, FileTransferObject } from '../../../node_modules/@ionic-native/file-transfer';

/**
 * Generated class for the ParentProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parent-profile',
  templateUrl: 'parent-profile.html',
})
export class ParentProfilePage {

  first = "";
  last = "";
  schoolid = "";
  gpa = "";
  schedule_img = "";
  Tel = "";
  student_id = "";
  email = "wasan@gmail.com";
  userid: any;
  student_level: any;
  student_img: any;
  school_img: any;
  username: any;
  schoolname: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider: RestProvider, public http: HTTP,
    private camera: Camera,
    private transfer: FileTransfer,
    private loadingCtrl: LoadingController) {

    this.userid = navParams.get('userid');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentProfilePage');
  }


  ionViewDidEnter() {

    this.restProvider.getInfo(this.userid).subscribe(result => {
      // console.log(result);

      this.first = result.fname;
      this.last = result.lname;
      this.schoolid = result.student_id;
      this.gpa = result.GPA;
      this.schedule_img = result.schedule_img;
      this.student_level = result.student_level;
      this.student_img = result.student_img
      this.school_img = result.schoolimg;
      this.username = result.username;
      this.schoolname = result.schoolname;
      this.Tel = result.Tel;
    });
  }

  home() {
    this.navCtrl.setRoot("HomePage");
  }
  back() {
    this.navCtrl.setRoot("HomePage");
  }

  changeProfileImg() {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      let loader = this.loadingCtrl.create({
        content: 'กำลังอัพโหลด'
      });
      loader.present();
      const fileTransfer: FileTransferObject = this.transfer.create();
      fileTransfer.upload(base64Image, 'http://13.250.56.205/du_service/api/uplodeImgProfile').then((data) => {

        loader.dismiss()
        let profileUrl = data.response
        profileUrl = profileUrl.substr(1).slice(0, -1);
        console.log('profileurl', profileUrl);

        this.restProvider.updateProfile(this.userid, profileUrl).subscribe((result) => {
          this.navCtrl.setRoot('HomePage')
        })

      })

    }, (err) => {
      console.log(err);

    });
  }



}
