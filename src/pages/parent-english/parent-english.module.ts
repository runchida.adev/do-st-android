import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentEnglishPage } from './parent-english';

@NgModule({
  declarations: [
    ParentEnglishPage,
  ],
  imports: [
    IonicPageModule.forChild(ParentEnglishPage),
  ],
})
export class ParentEnglishPageModule {}
