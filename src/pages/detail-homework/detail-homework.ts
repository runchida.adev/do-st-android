import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomeworkProvider } from '../../providers/homework/homework'
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the DetailHomeworkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-homework',
  templateUrl: 'detail-homework.html',
})
export class DetailHomeworkPage {


  homeworkId: any
  courseId: any
  courseName: any
  linkFrom: any

  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;

  homeworkDetail: any

  homeworkTitle: any
  homeworkThumb: any
  sentDate: any

  constructor(public navCtrl: NavController,
    public navParams: NavParams, private storage: Storage,
    private iab: InAppBrowser,
    private homeworkProvider: HomeworkProvider,
    private alertCtrl: AlertController) {

    this.homeworkId = navParams.get('homeworkid')
    this.courseId = navParams.get('courseid')

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailHomeworkPage');
  }

  ionViewDidEnter() {
    // console.log('homeworkId', this.homeworkId);
    // console.log('courseId', this.courseId);
    // console.log('linkfrom', this.linkFrom);


    this.storage.get('userid').then((val) => {
      this.userid = val;
      this.storage.get('schoolid').then((val) => {
        this.schoolid = val;
        this.storage.get('student_img').then((val) => {
          this.student_img = val;
          this.storage.get('student_level').then((val) => {
            this.student_level = val;
            console.log('school id:', this.schoolid);
            console.log('user id:', this.userid);
            console.log('course id:', this.courseId);
            console.log('homework id:', this.homeworkId);

            this.homeworkProvider.getHomeworkDetail(this.courseId, this.userid, this.schoolid, this.homeworkId).subscribe(result => {
              console.log('get homework detail response', result);
              this.homeworkDetail = result[0].Detail
              this.homeworkTitle = result[0].homeworkName
              this.homeworkThumb = result[0].doc
              this.sentDate = result[0].due_date
            })
          })
        })
      })
    })
  }

  back() {
    console.log('click from', this.linkFrom);

    // this.navCtrl.setRoot("EnglishPage");
  }

  save(urlImg) {
    let browser = this.iab.create(urlImg, '_blank', {
      closebuttoncaption: "Exit",
    });
  }

  sendHomework() {
    this.homeworkProvider.sentHomeWork(this.userid, this.homeworkId).subscribe(result => {
      console.log('send homework complete response', result);
      this.presentAlert();
      if (result == 'success') {
        this.navCtrl.setRoot('AllHomeworkPage')
      }

    })
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      message: 'กดส่งการบ้านแล้ว',
      buttons: ['OK']
    });

    await alert.present();
  }

}
