import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the VerbPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verb',
  templateUrl: 'verb.html',
})
export class VerbPage {

  height: any;
  width: any;
  verbId: any
  student_level: string;
  student_img: string;
  id: any;
  title: any
  verb_img: any

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage, public restProvider: RestProvider,
    public platform: Platform) {

    this.id = navParams.get('verbId')
    this.width = '100%';

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerbPage');
  }

  ionViewDidEnter() {
    console.log('verbId', this.id);

    this.storage.get('student_img').then((val) => {
      this.student_img = val;
      this.storage.get('student_level').then((val) => {
        this.student_level = val;
        this.restProvider.getV3ById(this.id).subscribe(result => {

          this.title = result[0].title
          this.verb_img = result[0].detail
        })
      })
    })
  }

  back() {
    this.navCtrl.setRoot('EnglishPage');
  }





}
