import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerbPage } from './verb';

@NgModule({
  declarations: [
    VerbPage,
  ],
  imports: [
    IonicPageModule.forChild(VerbPage),
  ],
})
export class VerbPageModule {}
