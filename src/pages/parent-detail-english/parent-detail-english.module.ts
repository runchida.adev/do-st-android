import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentDetailEnglishPage } from './parent-detail-english';

@NgModule({
  declarations: [
    ParentDetailEnglishPage,
  ],
  imports: [
    IonicPageModule.forChild(ParentDetailEnglishPage),
  ],
})
export class ParentDetailEnglishPageModule {}
