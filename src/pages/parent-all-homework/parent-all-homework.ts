import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { HomeworkProvider } from '../../providers/homework/homework';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ParentAllHomeworkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parent-all-homework',
  templateUrl: 'parent-all-homework.html',
})
export class ParentAllHomeworkPage {

  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;
  homeworkList: any;
  renderList: Array<any> = []

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage,
    public homeworkProvider: HomeworkProvider,
    public loadingController: LoadingController) {
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AllHomeworkPage');
    this.storage.get('userid').then((val) => {
      this.userid = val;
      this.storage.get('schoolid').then((val) => {
        this.schoolid = val;
        this.storage.get('student_img').then((val) => {
          this.student_img = val;
          this.storage.get('student_level').then((val) => {
            this.student_level = val;
            console.log(this.userid);
            console.log(this.schoolid);
            this.homeworkProvider.getHomeworkList(this.userid, this.schoolid).subscribe((result) => {
              console.log('all homework list', result);
              this.homeworkList = result
              this.homeworkList.forEach(element => {
                if (element.isSent == 'n') {
                  this.renderList.push(element)
                }
              });
            })
          })
        })
      })
    })
  }

  detailHomework(homeworkId, courseId) {
    console.log('prepare go to detail homework id', homeworkId);
    this.navCtrl.push("ParentDetailHomeworkPage", { 'homeworkid': homeworkId, 'courseid': courseId })
  }

}
