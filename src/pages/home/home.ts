import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { new_list } from '../../models/news_list';
import { RestProvider } from '../../providers/rest/rest';
import { HTTP } from '@ionic-native/http';
import { Storage } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  list_news: new_list;
  pic_news1: string;
  pic_news2: string;
  new1: string;
  new2: string;
  schoolid: string;
  newsid1: string;
  newsid2: string;
  first: string;
  last: string;
  gpa: string;
  schedule_img: string;
  student_level: string;
  student_img: string;
  userid: string;
  school_img: string;
  school_name: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider: RestProvider, public http: HTTP,
    private storage: Storage, public platform: Platform,
    private iab: InAppBrowser) {
  }

  ionViewDidLoad() {

    this.storage.get('userid').then((val) => {
      this.userid = val
      this.storage.get('token_st').then((val) => {

        if (val != "") {
          this.restProvider.UpdateToken_st(this.userid, val).subscribe(result => {
            console.log(result);
          });
        }


      })

      console.log('user id didenter', this.userid);
      this.getData();
    })



  }


  getData() {
    // this.storage.get('userid').then((val) => {
    // this.userid = val;
    console.log('get data user id', this.userid);
    this.restProvider.getInfo(this.userid).subscribe(result => {
      console.log(result);
      this.first = result.fname;
      this.last = result.lname;
      this.schoolid = result.schoolid;
      this.gpa = result.GPA;
      this.schedule_img = result.schedule_img;
      this.student_level = result.student_level;
      this.student_img = result.student_img
      console.log('student img', result.student_img);

      this.school_img = result.schoolimg;
      this.school_name = result.school_name;
      // console.log('school img',result.schoolimg);

      this.storage.set('schoolid', result.schoolid);
      this.storage.set('student_img', this.student_img);
      this.storage.set('student_level', result.student_level);
      this.storage.set('grade', result.grade);
      this.storage.set('roomid', result.roomid)


      this.storage.get('schoolid').then((val) => {
        this.schoolid = val;
        this.restProvider.gettop5_news(this.schoolid, "st", result.grade).subscribe(result => {
          this.list_news = result;
          console.log(result);
        });
      })

    });
  }

  profile() {
    this.navCtrl.push("ProfilePage", { 'userid': this.userid });
  }

  news(newsid, news_type, news_detail) {

    if (news_type == "survey") {
      let browser = this.iab.create('http://13.250.56.205/doedu/survey.aspx?schoolid=' + this.schoolid +
        '&userid=' + this.userid + '&surveyid=' + news_detail + '&who=st', '_blank', {
          closebuttoncaption: "Exit",
        });
      // this.iab.create();
    }

    else {
      this.navCtrl.push("DetailNewsPage", {
        'schoolid': this.schoolid,
        'newsid': newsid
      });
    }

  }

  schedule() {
    this.navCtrl.push("SchedulePage")
  }


}
