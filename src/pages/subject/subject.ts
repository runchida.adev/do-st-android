import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { course_list } from '../../models/course_list';
import { RestProvider } from '../../providers/rest/rest';
import { HTTP } from '../../../node_modules/@ionic-native/http';

/**
 * Generated class for the SubjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subject',
  templateUrl: 'subject.html',
})
export class SubjectPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider: RestProvider, public http: HTTP) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubjectPage');
  }

  cList: course_list;
  ionViewDidEnter() {
    this.restProvider.getStatus("1", "1").subscribe(result => {
      console.log(JSON.stringify(result));
      this.cList = result;
    });
  }

  eng() {
    this.navCtrl.push("EnglishPage");
  }



}
