import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ProvidersNewsProvider } from '../../providers/providers-news/providers-news';

/**
 * Generated class for the DetailNewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-news',
  templateUrl: 'detail-news.html',
})
export class DetailNewsPage {

  title: any;
  image: any;
  detail: any;
  schoolid: any;
  newsid: any;

  userid: any;
  student_level: string;
  student_img: string;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public _service: ProvidersNewsProvider,
    private storage: Storage) {

    this.schoolid = navParams.get('schoolid');
    this.newsid = navParams.get('newsid');
    this.storage.get('student_img').then((val) => {
      this.student_img = val
      this.storage.get('student_level').then((val) => {
        this.student_level = val
      })
    })


  }

  ionViewDidEnter() {
    this.storage.get('student_img').then((val) => {
      this.student_img = val;
      this.storage.get('student_level').then((val) => {
        this.student_level = val;
        this.storage.get('userid').then((val) => {
          this.userid = val;
          this.storage.get('schoolid').then((val) => {
            this.schoolid = val;
          })
        })
      })
    })
  }

  ionViewDidLoad() {
    this._service.getNewsDetail(this.schoolid, this.newsid).subscribe(result => {
      this.title = result[0]["title"];
      this.image = result[0]["news_img"];
      this.detail = result[0]["news_detail"];
    });
  }

}
