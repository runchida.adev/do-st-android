import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import { new_list } from '../../models/news_list';
import { course_list } from '../../models/course_list';
import { assignment_list } from '../../models/assignment_list';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {

  constructor(public http: HttpClient) {
    // console.log('Hello RestProvider Provider');
  }


  login(username: string, password: string): Observable<any> {

    let URL = 'http://13.250.56.205/du_service/api/login';
    let body = {
      "username": username,
      "password": password
    };

    console.log(body);

    return this.http.post<any>(URL, body);
  }


  getInfo(id: string): Observable<any> {

    let URL = 'http://13.250.56.205/du_service/api/getprofile';
    let body = {
      "userid": id,

    };
    return this.http.post<any>(URL, body);
  }

  uploadImg(fileItem) {
    let URL = 'http://13.250.56.205/du_service/api/uplodeImgProfile';
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    console.log('uploadFormData', JSON.stringify(uploadFormData));

    console.log('body', JSON.stringify(body));

    return this.http.post(URL, body)
  }


  uploadLocation(userid: string, latitude: string, longtitude: string): Observable<any> {

    let URL = 'http://13.250.56.205/du_service/api/update_profile_location';
    let body = {
      "userid": userid,
      "latitude": latitude,
      "longtitude": longtitude,
    };

    console.log('body', JSON.stringify(body));

    return this.http.post<any>(URL, body);
  }


  updateProfile(userid, profileUrl) {
    let URL = 'http://13.250.56.205/du_service/api/update_profile_img';
    let body = {
      userid: userid,
      url: profileUrl
    }
    console.log('update url', URL);

    return this.http.post(URL, body)
  }

  getSchedule(id: string): Observable<any> {

    let URL = 'http://13.250.56.205/du_service/api/getSchedule';
    let body = {
      "userid": id,
    };
    return this.http.post<any>(URL, body);
  }

  getNews(id: string, who: string, grade): Observable<new_list> {

    let URL = 'http://13.250.56.205/du_service/api/getnews';
    let body = {
      "schoolid": id,
      "who": who,
      "grade": grade,
    };
    return this.http.post<new_list>(URL, body);
  }

  gettop5_news(id: string, who: string, grade): Observable<any> {

    let URL = 'http://13.250.56.205/du_service/api/gettop5_news';
    let body = {
      "schoolid": id,
      "who": who,
      "grade": grade
    };
    console.log(body);
    return this.http.post<any>(URL, body);
  }

  UpdateToken_st(userid: string, token_st: string): Observable<any> {

    let URL = 'http://13.250.56.205/du_service/api/UpdateToken_st';
    let body = {
      "userid": userid,
      "token_st": token_st,
    };
    return this.http.post<any>(URL, body);
  }

  UpdateToken_pa(userid: string, token_pa: string): Observable<any> {

    let URL = 'http://13.250.56.205/du_service/api/UpdateToken_pa';
    let body = {
      "userid": userid,
      "token_pa": token_pa,
    };
    return this.http.post<any>(URL, body);
  }

  getCourseStatus(idUser: string, idSchool: string): Observable<course_list> {
    let URL = 'http://13.250.56.205/du_service/api/getCourseStatus';
    let body = {
      "userid": idUser,
      "schoolid": idSchool
    };
    return this.http.post<course_list>(URL, body);
  }

  getCourseList(userid: string, schoolid: string): Observable<course_list> {
    let URL = 'http://13.250.56.205/du_service/api/getCourseList';
    let body = {
      "userid": userid,
      "schoolid": schoolid
    };
    return this.http.post<course_list>(URL, body);
  }

  getStatus(idUser: string, idSchool: string): Observable<course_list> {

    let URL = 'http://13.250.56.205/du_service/api/getCourseStatus';
    let body = {
      "userid": idUser,
      "schoolid": idSchool
    };
    return this.http.post<course_list>(URL, body);
  }

  getAssignment(idUser: string): Observable<assignment_list> {

    let URL = 'http://13.250.56.205/du_service/api/getAssignmentByuserid';
    let body = {
      "userid": idUser
    };
    return this.http.post<assignment_list>(URL, body);
  }

  resetpassword(username: string, tel: string): Observable<any> {
    let URL = 'http://13.250.56.205/du_service/api/request_forget_password';
    let body = {
      "tel": tel,
      "username": username
    };
    return this.http.post(URL, body);
  }

  getV3ById(id: string): Observable<any> {
    let URL = 'http://13.250.56.205/du_service/api/getV3ByID'
    let body = {
      id: id
    }
    return this.http.post<any>(URL, body)
  }

  getV3List(courseid, roomid): Observable<any> {
    let URL = 'http://13.250.56.205/du_service/api/getV3List'
    let body = {
      "courseid": courseid,
      "roomid": roomid
    }
    return this.http.post<any>(URL, body)
  }

  getVocabList(courseid, roomid): Observable<any> {
    let URL = 'http://13.250.56.205/du_service/api/getVocabList'
    let body = {
      "courseid": courseid,
      "roomid": roomid
    }
    return this.http.post<any>(URL, body)
  }

  getVocabListDetail(assign_id): Observable<any> {
    let URL = 'http://13.250.56.205/du_service/api/getVocabListDetail'
    let body = {
      "assign_id": assign_id
    }
    return this.http.post<any>(URL, body)
  }

  getNoti(userid) {
    let URL = 'http://13.250.56.205/du_service/api/getNoti_st'
    let body = {
      userid: userid
    }
    return this.http.post(URL, body)
  }

  saveHomework(courseid, schoolid, createid, updateid, homeworkName, dateOfEvent
    , detail, homework_img, alert_before, roomid, isactive): Observable<any> {
    let URL = 'http://13.250.56.205/du_service/api/addHomework'
    let body = {
      "homeworkName": homeworkName,
      "isactive": isactive,
      "schoolid": schoolid,
      "createid": createid,
      "updateid": updateid,
      "due_date": dateOfEvent,
      "courseid": courseid,
      "detail": detail,
      "roomid": roomid,
      "doc": homework_img,
      "alert_before": alert_before

    }
    console.log('body', JSON.stringify(body));
    return this.http.post<any>(URL, body)
  }

  tmppictureHomework(url_img, userid): Observable<any> {
    let URL = 'http://13.250.56.205/du_service/api/addImgHomework_tmp'
    let body = {
      "url_img": url_img,
      "userid": userid
    }
    return this.http.post<any>(URL, body)
  }

  change_password(username: string, tel: string, newpassword: string): Observable<any> {
    let URL = 'http://13.250.56.205/du_service/api/change_password';
    let body = {
      "tel": tel,
      "username": username,
      "newpassword": newpassword
    };
    return this.http.post(URL, body);
  }

  change_password_pa(username: string, tel: string, newpassword: string): Observable<any> {
    let URL = 'http://13.250.56.205/du_service/api/change_password_pa';
    let body = {
      "tel": tel,
      "username": username,
      "newpassword": newpassword
    };
    return this.http.post(URL, body);
  }





}
