import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'

/*
  Generated class for the SdqProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SdqProvider {

  constructor(public http: HttpClient) {
    console.log('Hello SdqProvider Provider');
  }

  getQus(userid: string): Observable<any> {
    let URL = 'http://13.250.56.205/du_service/api/getSDQ_question';
    let body = {
      "userid": userid
    };
    return this.http.post<any>(URL, body);
  }

  getQus_pa(userid: string): Observable<any> {
    let URL = 'http://13.250.56.205/du_service/api/getSDQ_question_pa';
    let body = {
      "userid": userid
    };
    return this.http.post<any>(URL, body);
  }

  clickAns(studentid: string, updateid: string, schoolid: string, question_number: string, ans: string): Observable<any> {

    let URL = 'http://13.250.56.205/du_service/api/InsertSDQ_question';
    let body = {
      "studentid": studentid,
      "updateid": updateid,
      "schoolid": schoolid,
      "question_number": question_number,
      "ans": ans
    };
    return this.http.post<any>(URL, body);
  }

  clickAns_pa(studentid: string, updateid: string, schoolid: string, question_number: string, ans: string): Observable<any> {

    let URL = 'http://13.250.56.205/du_service/api/InsertSDQ_question_pa';
    let body = {
      "studentid": studentid,
      "updateid": updateid,
      "schoolid": schoolid,
      "question_number": question_number,
      "ans": ans
    };
    return this.http.post<any>(URL, body);
  }

}
