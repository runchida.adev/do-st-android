import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ProvidersNewsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProvidersNewsProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ProvidersNewsProvider Provider');
  }

  getNewsDetail(schoolid, newsid) {
    let URL = 'http://13.250.56.205/du_service/api/getNews_detail'
    let body = {
      schoolid: schoolid,
      newsid: newsid
    }
    return this.http.post(URL, body)
  }

}
