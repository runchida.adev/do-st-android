import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the CheckNameProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CheckNameProvider {

  constructor(public http: HttpClient) {
    console.log('Hello CheckNameProvider Provider');
  }

  getInfo(schoolid, courseid, roomid, password): Observable<any> {

    let URL = 'http://13.250.56.205/du_service/api/getCheck_TA_USER';
    let body = {

      "schoolid": schoolid,
      "courseid": courseid,
      "roomid": roomid,
      "password": password

    };

    console.log(body)
    return this.http.post<any>(URL, body);
  }


  getStudentList_chk(roomid, teacherid, courseid): Observable<any> {

    let URL = 'http://13.250.56.205/du_service/api/getStudentList_chk';
    let body = {

      "roomid": roomid,
      "teacherid": teacherid,
      "courseid": courseid

    };

    console.log(body)
    return this.http.post<any>(URL, body);
  }

  UpdateCheckName_class(id, IsComing, update_id): Observable<any> {

    let URL = 'http://13.250.56.205/du_service/api/UpdateCheckName_class';
    let body = {

      "id": id,
      "IsComing": IsComing,
      "update_id": update_id

    };

    console.log(body)
    return this.http.post<any>(URL, body);
  }

}
