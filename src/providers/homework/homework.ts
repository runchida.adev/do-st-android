import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the HomeworkProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HomeworkProvider {

  constructor(public http: HttpClient) {
    console.log('Hello HomeworkProvider Provider');
  }

  getHomework(courseid, userid, schoolid) {
    let URL = 'http://13.250.56.205/du_service/api/getHomework'
    let body = {
      courseid: courseid,
      userid: userid,
      schoolid: schoolid
    }
    return this.http.post(URL, body)
  }


  sentHomeWork(userid, id) {
    let URL = 'http://13.250.56.205/du_service/api/sentHomeWork'
    let body = {
      userid: userid,
      id: id
    }
    console.log(body)
    return this.http.post(URL, body)
  }

  getScore(courseid: string, userid: string): Observable<any> {
    let URL = 'http://13.250.56.205/du_service/api/getScore';
    let body = {
      "courseid": courseid,
      "userid": userid
    };
    return this.http.post<any>(URL, body);
  }
  getAllScore(userid, schoolid, roomid, courseid) {
    let URL = 'http://13.250.56.205/du_service/api/getScore_all'
    let body = {
      studentid: userid,
      schoolid: schoolid,
      roomid: roomid,
      courseid: courseid
    }
    return this.http.post(URL, body)
  }

  getHomeworkList(userid, schoolid) {
    let URL = 'http://13.250.56.205/du_service/api/getHomeworkAll'
    let body = {
      userid: userid,
      schoolid: schoolid
    }
    return this.http.post(URL, body);
  }

  getHomeworkDetail(courseid, userid, schoolid, homeworkid) {
    let URL = 'http://13.250.56.205/du_service/api/getHomework_detail'
    let body = {
      courseid: courseid,
      userid: userid,
      schoolid: schoolid,
      homeworkid: homeworkid
    }
    return this.http.post(URL, body)
  }

}
