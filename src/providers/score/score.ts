import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'

/*
  Generated class for the ScoreProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ScoreProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ScoreProvider Provider');
  }

  getStd(courseid: string, schoolid: string, roomid: string, teacherid: string): Observable<any> {
    let URL = 'http://13.250.56.205/du_service/api/getSubScoreList_SeeAll';
    let body = {
      "courseid": courseid,
      "schoolid": schoolid,
      "roomid": roomid,
      "teacherid": teacherid

    };
    return this.http.post<any>(URL, body);
  }

  getScoreClassAll(sub_score_id: string, schoolid: string): Observable<any> {
    let URL = 'http://13.250.56.205/du_service/api/getSubScoreDetail_list';
    let body = {
      "sub_score_id": sub_score_id,
      "schoolid": schoolid,
    };
    return this.http.post<any>(URL, body);
  }

}
