export interface assignment_list {
    assignment_id: string;
    assignment_detail: string;
    course_name: string;
    assignment_status: string;
    create_date: string;
    returncode: string;
    desc: string;
}