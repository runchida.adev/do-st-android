export interface new_list {
    newsid: string;
    title: string;
    news_img: string;
    news_detail: string;
    IsActive: string;
    schoolid: string;
    returncode: string;
    desc: string;
}