export interface course_list {
    courseid: string;
    course_name: string;
    class_status: string;
    returncode: string;
    desc: string;
}